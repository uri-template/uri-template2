;;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;;; SPDX-License-Identifier: GPL-3.0-or-later

;;; This file is part of uri-template2.

;;; uri-template2 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.

;;; uri-template2 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with uri-template2. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package #:uri-template2)

(parenscript:defpsmacro uri-encode (x)
  `(encode-u-r-i-component ,x))

(parenscript:defpsmacro uri-template-to-string (&rest template-args)
  `(+ ,@template-args))
