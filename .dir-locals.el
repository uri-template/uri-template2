;;; Directory Local Variables
;;; For more information see (info "(emacs) Directory Variables")

((nil
  (indent-tabs-mode)
  (fill-column . 69))
 (lisp-mode
  (eval put 'test 'common-lisp-indent-function
        1)
  ))
