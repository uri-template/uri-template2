;;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;;; SPDX-License-Identifier: GPL-3.0-or-later

;;; This file is part of uri-template2.

;;; uri-template2 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.

;;; uri-template2 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with uri-template2. If not, see
;;; <https://www.gnu.org/licenses/>.

(defpackage #:uri-template2/test
  (:use #:cl #:uri-template2 #:named-readtables #:fiveam)
  (:export #:run-tests #:suite-failure))

(in-package #:uri-template2/test)

(in-readtable uri-template)

(define-condition suite-failure (warning)
  ((suite        :initarg :suite
                 :reader  suite-failure-suite)
   (result-list  :initarg :result-list
                 :reader  suite-failure-result-list))
  (:report (lambda (condition s)
             (write-string "Error in suite " s)
             (prin1 (suite-failure-suite condition) s)
             (terpri s)
             (let ((fiveam:*print-names*  nil)
                   (fiveam:*test-dribble* s))
               (fiveam:explain! (suite-failure-result-list condition))))))

(defun run-tests ()
  (let ((test-results (fiveam:run :uri-template2)))
    (or (fiveam:explain! test-results)
        (signal 'suite-failure
                :suite       :uri-template2
                :result-list test-results))))

(in-suite* :uri-template2)

(test interpolation3
  (let ((bar "bar")
        (baz 1))
   (is (string= #U"http://www.foo.com/bar/{bar}{baz}"
                "http://www.foo.com/bar/bar1"))))

(test interpolation2
  (let ((bar "bar")
        (baz 1))
    (is (string= #U"http://www.foo.com/bar/{bar}/{baz}"
                 "http://www.foo.com/bar/bar/1"))))

(test interpolation1
  (let ((baz 1))
    (is (string= #U"http://www.foo.com/bar/{baz}"
                 "http://www.foo.com/bar/1"))))

(test interpolation-encoding
  (is (string= #U"http://www.foo.com/bar?foo={"^BAZ !bar"}"
               "http://www.foo.com/bar?foo=%5EBAZ%20%21bar")))

(test destructuring1
  (is (equal
       (uri-template-bind (#U"http://www.factory.com/orders/{part}/{number}")
           "http://www.factory.com/orders/widget/1234"
         (list part (parse-integer number) %uri-host))
       '("widget" 1234 "www.factory.com"))))

(test destructuring2
  (is
   (equal
    (uri-template-bind (#U"{uri}")
        "https://www.google.com/dir/1/2/search.html?arg=0-a&arg1=1-b&amp;arg3-c#hash"
      (list %uri-scheme %uri-host %uri-path %uri-directory %uri-file %uri-query %uri-fragment))
    '("https" "www.google.com" "/dir/1/2/search.html" "/dir/1/2/" "search.html" "arg=0-a&arg1=1-b&amp;arg3-c" "hash"))))

(test destructuring3
  (is (equal (uri-template-bind (#U"/apps/{app-name}/{table-template}/{table-id}")
                 "/apps/EVWeb/Fixed%20Cost/20"
               (list app-name table-template table-id %uri-path))
             '("EVWeb" "Fixed Cost" "20" "/apps/EVWeb/Fixed Cost/20"))))

(test destructuring4
  (is
   (equal
    (uri-template-bind (#U"{uri}")
        "http://files3.dsv.data.cod.ru/?WyIyNWQ3NWU5NTRmZDU1MWIzYmQ5NzVjNzJhZjRkZmNhZSIsMTI1MTA5NjMxNCwiXHUwNDEwXHUwNDNiXHUwNDRjXHUwNDRmXHUwNDNkXHUwNDQxIFx1MDQ0MVx1MDQzNVx1MDQ0MFx1MDQzZVx1MDQzYVx1MDQ0MFx1MDQ0Ylx1MDQzYlx1MDQ0Ylx1MDQ0NS5yYXIiLCJrTzNqSUo3bUN5WlBPenlBVGdcL0M3UkZVWHdXYkN6SWtEYzUweTl5a1lOVCtTRmlwVFdsN1UxWlVybGVLNjMyaGlYc0hvVDhGZitGWUt6eGVVRGxOVkxUN3R0MndLYjg4VGFjYmZSVnhrZjNYQXdZalpYemVEQXM4bmxzK0RCbnZEcnZQTmRMKytDS05pNjVJXC8yb2JnY0N1RmdyK1lpS0VSak8rNVZSeTIrcz0iXQ%3D%3D"
      (list %uri-scheme %uri-authority %uri-path %uri-directory %uri-file %uri-query %uri-fragment))
    '("http" "files3.dsv.data.cod.ru" "/" "/" nil "WyIyNWQ3NWU5NTRmZDU1MWIzYmQ5NzVjNzJhZjRkZmNhZSIsMTI1MTA5NjMxNCwiXHUwNDEwXHUwNDNiXHUwNDRjXHUwNDRmXHUwNDNkXHUwNDQxIFx1MDQ0MVx1MDQzNVx1MDQ0MFx1MDQzZVx1MDQzYVx1MDQ0MFx1MDQ0Ylx1MDQzYlx1MDQ0Ylx1MDQ0NS5yYXIiLCJrTzNqSUo3bUN5WlBPenlBVGdcL0M3UkZVWHdXYkN6SWtEYzUweTl5a1lOVCtTRmlwVFdsN1UxWlVybGVLNjMyaGlYc0hvVDhGZitGWUt6eGVVRGxOVkxUN3R0MndLYjg4VGFjYmZSVnhrZjNYQXdZalpYemVEQXM4bmxzK0RCbnZEcnZQTmRMKytDS05pNjVJXC8yb2JnY0N1RmdyK1lpS0VSak8rNVZSeTIrcz0iXQ==" nil))))

(test destructuring5
  (is
   (equal
    (uri-template-bind (#U"{uri}")
        "http://www.foo.com/abc?bar=baz&xyz=1#hash"
      (list %uri-scheme %uri-host %uri-path %uri-directory %uri-file %uri-query %uri-fragment %uri-head %uri-tail))
    '("http" "www.foo.com" "/abc" "/" "abc" "bar=baz&xyz=1" "hash" "http://www.foo.com" "/abc?bar=baz&xyz=1#hash"))))

(test destructuring6
  (is
   (equal
    (uri-template-bind (#U"{uri}")
        "http://user@www.foo.com:8080/dir/abc?bar=baz&xyz=1#hash"
      (list %uri-scheme %uri-authority %uri-user %uri-host %uri-port %uri-path %uri-directory %uri-file %uri-query %uri-fragment %uri-head %uri-tail))
    '("http" "user@www.foo.com:8080" "user" "www.foo.com" "8080" "/dir/abc" "/dir/" "abc" "bar=baz&xyz=1" "hash" "http://user@www.foo.com:8080" "/dir/abc?bar=baz&xyz=1#hash"))))

(test destructuring7
  (is
   (equal
    (uri-template-bind (#U"{uri}")
        "http://www.foo.com/?bar=baz&xyz=1"
      (list %uri-scheme %uri-host %uri-path %uri-directory %uri-file %uri-query %uri-fragment))
    '("http" "www.foo.com" "/" "/" nil "bar=baz&xyz=1" nil))))

(test destructuring8
  (is
   (equal
    (uri-template-bind (#U"{uri}") "http://user@host.com:8080/dir1/dir2/file?query=want&a=b#hash"
      (list %uri-scheme %uri-authority (list %uri-user %uri-host %uri-port)
            %uri-path (list %uri-directory %uri-file) %uri-query %uri-fragment))
    '("http" "user@host.com:8080" ("user" "host.com" "8080") "/dir1/dir2/file"
      ("/dir1/dir2/" "file") "query=want&a=b" "hash"))))

(test destructuring9
  (is
   (equal
    (uri-template-bind (#U"{uri}") "/foo/bar"
      (list %uri-head %uri-tail %uri-scheme
            %uri-authority (list %uri-user %uri-host %uri-port)
            %uri-path (list %uri-directory %uri-file) %uri-query %uri-fragment))
    '(NIL "/foo/bar" NIL NIL (NIL NIL NIL) "/foo/bar" ("/foo/" "bar") NIL NIL))))

(test encoding1
  (is (string= "abc123" #U"{"abc123"}")))

(test encoding2
  (is (string= "abc%20123" #U"{"abc 123"}")))

(test decoding3
  (is (string= "abc123" (uri-template-bind (#U"{x}") "abc123" x))))

(test decoding4
  (is (string= "abc 123" (uri-template-bind (#U"{x}") "abc%20123" x))))

(test unicode-encoding1
  (is (string= "%D1%84%D0%BE%D0%BE" #U"{"фоо"}")))

(test unicode-decoding2
  (is (string= "бар"
               (uri-template-bind (#U"{x}") "%D0%B1%D0%B0%D1%80" x))))

(test null-template
  (is (string= #U"" "")))

(test delimited-template-before-string-no-whitespace
  (is (equal (list #U"/foobar""baz")
             '("/foobar" "baz"))))

