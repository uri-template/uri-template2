;;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;;; SPDX-License-Identifier: GPL-3.0-or-later

;;; This file is part of uri-template2.

;;; uri-template2 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.

;;; uri-template2 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with uri-template2. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package #:cl)

(defpackage #:uri-template2
  (:use #:cl #:cl-ppcre #:named-readtables)
  (:export
   ;; readtable
   #:uri-template

   ;; interpolation
   #:read-uri-template

   ;; destructuring
   #:uri-template-bind

   ;; RFC 2396 standard URI components
   #:%uri-scheme
   #:%uri-authority
   #:%uri-path
   #:%uri-query
   #:%uri-fragment

   ;; extended components
   #:%uri-head
   #:%uri-tail
   #:%uri-user
   #:%uri-host
   #:%uri-port
   #:%uri-directory
   #:%uri-file))

(in-package #:uri-template2)

(defreadtable uri-template
  (:merge :standard)
  (:dispatch-macro-char #\# #\U
                        (lambda (&rest args)
                          (apply #'uri-template-reader args))))
