;;; -*- Mode: LISP; Syntax: COMMON-LISP; Base: 10; indent-tabs-mode: nil -*-

(defsystem "uri-template2"
  :description "An implementation of the URI Template proposed standard draft version 01."
  :long-description "An implementation of the URI Template proposed standard draft version 01. Lets you easily create and parse URIs by using the URI Template reader macro syntax."
  :author "Vladimir Sedach <vas@oneofus.la>"
  :license "GPL-3.0-or-later"
  :depends-on ("cl-ppcre" "named-readtables" "percent-encoding")
  :serial t
  :components ((:file "package")
               (:file "uri-template")
               (:file "destructure-uri")
               #+parenscript (:file "parenscript-implementation")
               )
  :in-order-to ((test-op (test-op "uri-template2/test"))))

(defsystem "uri-template2/test"
  :license "GPL-3.0-or-later"
  :depends-on ("uri-template2" "fiveam")
  :serial t
  :components ((:file "test"))
  :perform (test-op (o c)
             (uiop:symbol-call :uri-template2/test '#:run-tests)))
