;;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;;; SPDX-License-Identifier: GPL-3.0-or-later

;;; This file is part of uri-template2.

;;; uri-template2 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.

;;; uri-template2 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with uri-template2. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package #:uri-template2)

(defun read-uri-template (stream &optional recursive-p)
  "A function suitable for inserting into the readtable so you can
read URI templates from your own dispatch character."
  (flet ((make-acc (&optional (type 'character))
           (make-array 0 :adjustable t :fill-pointer t
                         :element-type type)))
    (let ((template (make-acc t))
          (str-acc  (make-acc)))
      (flet ((collect-string ()
               (when (< 0 (length str-acc))
                 (vector-push-extend str-acc template)
                 (setf str-acc (make-acc)))))
        (multiple-value-bind (original1 original2)
            (get-macro-character #\})
          (unwind-protect
               (progn
                 (set-syntax-from-char #\} #\))
                 (do ((next-char
                       (assert
                        (char= #\"
                               (read-char stream t nil recursive-p))
                        ()
                        "URI template needs to be delimited with double quotation marks. For example: #U\"http://www.website.com/pages/{pagenum}\"")))
                     ((char= #\"
                             (setf
                              next-char
                              (read-char stream t nil recursive-p)))
                      (collect-string))
                   (if (char= #\{ next-char)
                       (progn
                         (collect-string)
                         (vector-push-extend
                          `(uri-encode
                            (progn
                              ,@(read-delimited-list #\} stream)))
                          template))
                       (vector-push-extend next-char str-acc))))
            (set-macro-character #\} original1 original2))))
      (coerce template 'list))))

(defmacro uri-encode (x)
  (let ((x1 (gensym)))
    `(percent-encoding:encode
      (let ((,x1 ,x))
        (if (stringp ,x1)
            ,x1
            (princ-to-string ,x1))))))

(defun uri-template-to-string (&rest template-args)
  "The head of the list that the URI template reader produces. A
function or macro."
  (format nil "~{~A~}" template-args))

(defun uri-template-reader (stream subchar arg)
  (declare (ignore subchar arg))
  (cons 'uri-template-to-string (read-uri-template stream t)))
