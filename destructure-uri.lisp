;;; Copyright 2007-2012 Vladimir Sedach <vas@oneofus.la>

;;; SPDX-License-Identifier: GPL-3.0-or-later

;;; This file is part of uri-template2.

;;; uri-template2 is free software: you can redistribute it and/or
;;; modify it under the terms of the GNU General Public License as
;;; published by the Free Software Foundation, either version 3 of
;;; the License, or (at your option) any later version.

;;; uri-template2 is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
;;; General Public License for more details.

;;; You should have received a copy of the GNU General Public License
;;; along with uri-template2. If not, see
;;; <https://www.gnu.org/licenses/>.

(in-package #:uri-template2)

(defmacro weak-register-groups-bind (vars regex str &body body)
  `(destructuring-bind (&optional ,@vars)
       (coerce (nth-value 1 (scan-to-strings ,regex ,str)) 'list)
     (declare (ignorable ,@vars))
     (setf ,@(loop for var in vars append
                  `(,var (when ,var (percent-encoding:decode ,var)))))
     ,@body))

(defmacro uri-template-bind ((template) uri &body body)
  "Binds URI template placeholders (which must be symbols) in given
URI, as well as attempting to bind a set of standard URI components to
their respective parts of the given URI. Body executes only if all
explicitly specified URI template placeholders can be bound.

Given the example URI http://user@www.foo.com:8080/dir/abc?bar=baz&xyz=1#hash
The standard URI components look like:

%uri-scheme     http
%uri-authority  user@www.foo.com:8080
%uri-user       user
%uri-host       www.foo.com
%uri-port       8080
%uri-path       /dir/abc
%uri-directory  /dir/
%uri-file       abc
%uri-query      bar=baz&xyz=1
%uri-fragment   hash
%uri-head       http://user@www.foo.com:8080
%uri-tail       /dir/abc?bar=baz&xyz=1#hash"
  (assert (eq (car template) 'uri-template-to-string))
  (let* ((template (cdr template))
         (template-vars (mapcar (lambda (x)
                                  (car (last (second x))))
                                (remove-if #'stringp template)))
         (uri-var (gensym)))
    `(let ((,uri-var ,uri))
       (weak-register-groups-bind (%uri-head x1 %uri-scheme x2 %uri-authority %uri-tail %uri-path x3 %uri-query x4 %uri-fragment)
           ;; regex adapted from RFC 2396: "^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?"
           "^((([^:/?#]+):)?(//([^/?#]*)))?(([^?#]*)(\\?([^#]*))?(#(.*))?)"
           ,uri-var
         (weak-register-groups-bind (x1 %uri-user %uri-host x2 %uri-port)
             "(([^@]+)@)?([^\\:]+)(\\:(\\d+))?"
             %uri-authority
           (weak-register-groups-bind (%uri-directory %uri-file)
               "(.*/)([^/]+)?"
               %uri-path
             (weak-register-groups-bind ,template-vars
                 '(:sequence
                   :start-anchor
                   ,@(substitute-if
                      '(:register (:greedy-repetition 0 nil :everything))
                      (complement #'stringp)
                      template)
                   :end-anchor) ,uri-var
               (when (and ,@template-vars)
                 ,@body))))))))
